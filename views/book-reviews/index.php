<?php use yii\helpers\Url; ?>
<h1>Book Reviews List</h1>
<table class="table table-striped">
	
	<tr>
		<th>Title</th>
		<th>Author</th>
		<th>Action</th>
	</tr>
	
	<?php
		foreach ($booksList as $book) {
			$id = $book['id'];
	 ?>
	<tr>
		<th><?= $book['title'] ?></th>
		<th><?= $book['auther'] ?></th>
		<th><a href="<?= Url::to(['book-reviews/view', 'id' => $id]) ?>"> <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></th>
	</tr>
	<?php
	}
	?>
</table>