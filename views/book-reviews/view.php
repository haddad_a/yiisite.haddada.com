<?php
$this->params['breadcrumbs'][] = ['label' => 'book title', 'url' => '/book-reviews'];
$this->params['breadcrumbs'][] = $title;
?>
<h1>Review the book</h1>

<table class="table table-striped table-bordered">

    <tr>
       <td>Book Title</td>
       <td><?= $title?></td>
    </tr>

    <tr>
        <td>Author</td>
        <td><?= $auther ?></td>
    </tr>
</table>
