<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\Request;

class BookReviewsController extends Controller
{

	public function actionIndex() {
		$data['booksList'] = $this->actionGetBooksList();
		return $this->render('index',$data);
	}

	public function actionView($id) {
		$data['id'] = $id;
		$bookslist = $this->actionGetBooksList();
        foreach ($bookslist as $book){
            if ($id == $book['id']) {
                $data['auther'] = $book['auther'];
                $data['title'] = $book['title'];
            }
        }
		return $this->render('view', $data);
	}

	public function actionGetBooksList() {
		$booksList = [
			['id' => 1, 'title' => 'Web app', 'auther' => 'me', 'url' => 'www.amazon.com'],
            ['id' => 2, 'title' => 'yii2', 'auther' => 'myself', 'url' => 'www.amazon.com'],
            ['id' => 3, 'title' => 'mastering', 'auther' => 'And I', 'url' => 'www.amazon.com']
        ];
        return $booksList;
    }
}
